package com.dt3264.deezloader

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.media.MediaScannerConnection
import android.os.Build
import android.os.IBinder
import android.util.ArrayMap
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import io.socket.client.IO
import io.socket.client.Socket
import org.json.JSONObject
import kotlin.concurrent.withLock

class DeezloaderService : Service() {
    private var serviceSocket: Socket? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        createNotificationChannel()
        prepareNodeServerListeners()
        startService()

        if (!nodeThread.started) {
            Thread {
                nodeThread.start()
            }.apply { start() }
        }

        return START_NOT_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        // Used only in case of bound services.
        return null
    }

    override fun onDestroy() {
        if (serviceSocket != null) {
            serviceSocket!!.disconnect()
            serviceSocket = null
        }

        if (nodeThread.isAlive) {
            nodeThread.interrupt()
        }

        super.onDestroy()
    }

    // Notification methods
    private fun startService() {
        val serverNotifIntent = Intent(applicationContext, BrowserActivity::class.java).apply {
            action = Intent.ACTION_MAIN
            addCategory(Intent.CATEGORY_LAUNCHER)
            addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT or Intent.FLAG_ACTIVITY_SINGLE_TOP)
        }

        val serverPendingIntent = PendingIntent.getActivity(this, 0, serverNotifIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val serverNotifBuilder = NotificationCompat.Builder(this, serviceChannelID).apply {
            setContentTitle(resources.getString(R.string.app_name))
            setTicker(resources.getString(R.string.app_name))
            setContentText(resources.getString(R.string.notificationService))
            setSmallIcon(R.mipmap.ic_notification)
            setOngoing(false)
            setContentIntent(serverPendingIntent)
            setCategory(NotificationCompat.CATEGORY_SERVICE)
        }

        startForeground(serviceNotificationID, serverNotifBuilder.build())
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val thisNotificationManager = getSystemService(NotificationManager::class.java)!!

            val servicename = "Service Notification"
            val servicedescription = "The notification present when the Deezloader service is running."
            val serviceimportance = NotificationManager.IMPORTANCE_MIN
            val servicechannel = NotificationChannel(serviceChannelID, servicename, serviceimportance)
            servicechannel.description = servicedescription
            servicechannel.setSound(null, null)
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            thisNotificationManager.createNotificationChannel(servicechannel)

            val downloadname = "Download Notifications"
            val downloaddescription = "Notifications providing the status of music downloads."
            val downloadimportance = NotificationManager.IMPORTANCE_DEFAULT
            val downloadchannel = NotificationChannel(downloadChannelID, downloadname, downloadimportance)
            downloadchannel.description = downloaddescription
            downloadchannel.setSound(null, null)
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            thisNotificationManager.createNotificationChannel(downloadchannel)
        }
    }

    private fun prepareNodeServerListeners() {
        // Socket to catch emits from Remix
        serviceSocket = IO.socket(serverURL).apply {
            on("checkAutologin") {
                lock.withLock { condition.signalAll() }
            }

            on("addToQueue") { args ->
                val updateData = args[0] as JSONObject

                val name = updateData.get("name") as String
                val artist = updateData.get("artist") as String
                val size = updateData.get("size").toString()
                val queueId = updateData.get("queueId").toString()
                val type = updateData.get("type") as String

                newDownloadNotification(type, queueId, name, artist, size)
            }

            // This is used for overall progress on batched downloads
            on("updateQueue") { args ->
                val updateData = args[0] as JSONObject

                val size = updateData.get("size").toString()
                val downloaded = updateData.get("downloaded").toString()
                val failed = updateData.get("failed").toString()
                val queueId = updateData.get("queueId").toString()
                val type = updateData.get("type") as String

                if (queueId in notifs.keys) {
                    if (type != "track") {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            updateBatchDownloadSubtext(queueId, size, downloaded, failed)
                            if (downloaded.toInt() + failed.toInt() == size.toInt()) {
                                downloadEndNotification(queueId, "Download Complete")
                            }
                        } else {
                            if (downloaded.toInt() + failed.toInt() == size.toInt()) {
                                updateBatchDownloadSubtext(queueId, size, downloaded, failed)
                                downloadEndNotification(queueId, "Download Complete")
                            }
                        }
                    } else if (downloaded == size) {
                        downloadEndNotification(queueId, "Download Complete")
                    } else if (failed == size) {
                        downloadEndNotification(queueId, "Download Failed")
                    }
                }
            }

            // This is used for overall download progress on all download types
            on("downloadProgress") { args ->
                val updateData = args[0] as JSONObject

                val queueId = updateData.get("queueId").toString()
                var percentage = updateData.get("percentage").toString().toDouble().toInt()

                if (percentage < 0) {
                    percentage = 0
                }

                if (queueId in notifs.keys) {
                    if (percentage != 100) {
                        updateDownloadProgress(queueId, percentage)
                    }
                }
            }

            on("downloadComplete") { args ->
                scanNewSongInternal(args[0] as String)
            }

            open()
        }
    }

    private var notifId = 100
    private var notifs: ArrayMap<String, ArrayMap<Int, NotificationCompat.Builder>> = ArrayMap()

    private fun newDownloadNotification(type: String, queueId: String, name: String, artist: String, size: String, downloaded: String = "0", failed: String = "0") {
        val thisBuilder = NotificationCompat.Builder(baseContext, downloadChannelID).apply {
            setSmallIcon(R.mipmap.ic_notification)
            setContentTitle("$name by $artist")
            setProgress(0, 0, true)
            setOnlyAlertOnce(true)
            setTimeoutAfter(6000000)
            setOngoing(false)
        }

        if (type != "track" && Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            thisBuilder.apply {
                setSubText("$downloaded/$size downloaded, $failed failed")
            }
        }

        val thisBuilderMap: ArrayMap<Int, NotificationCompat.Builder> = ArrayMap()

        thisBuilderMap[notifId] = thisBuilder

        notifs[queueId] = thisBuilderMap
        notifId++
        NotificationManagerCompat.from(baseContext).notify(
            queueId,
            notifs.getValue(queueId).keyAt(0),
            thisBuilder.build()
        )
    }

    private fun updateBatchDownloadSubtext(queueId: String, size: String = "1", downloaded: String = "0", failed: String = "0") {
        val thisBuilder = notifs.getValue(queueId).getValue(notifs.getValue(queueId).keyAt(0))

        thisBuilder.apply {
            setSubText("$downloaded/$size downloaded, $failed failed")
        }

        NotificationManagerCompat.from(baseContext).notify(
            queueId,
            notifs.getValue(queueId).keyAt(0),
            thisBuilder.build()
        )
    }

    private fun updateDownloadProgress(queueId: String, progress: Int) {
        val thisBuilder = notifs.getValue(queueId).getValue(notifs.getValue(queueId).keyAt(0))

        thisBuilder.apply {
            setProgress(100, progress, false)
        }

        NotificationManagerCompat.from(baseContext).notify(
            queueId,
            notifs.getValue(queueId).keyAt(0),
            thisBuilder.build()
        )
    }

    private fun downloadEndNotification(queueId: String, reason: String) {
        val thisBuilder = notifs.getValue(queueId).getValue(notifs.getValue(queueId).keyAt(0))

        thisBuilder.apply {
            setProgress(0, 0, false)
            setContentText(reason)
            setOngoing(false)
        }

        NotificationManagerCompat.from(baseContext).notify(
            queueId,
            notifs.getValue(queueId).keyAt(0),
            thisBuilder.build()
        )

        notifs.remove(queueId)
    }

    // Function to add new songs to the mediastore once fully downloaded
    private fun scanNewSongInternal(filePath: String) {
        MediaScannerConnection.scanFile(this,
                arrayOf(filePath),
                null
        ) { _, _ -> }
    }
}
